# Implementation:

### Q) What libraries did you add to the frontend? What are they used for?
I used NextJs to create the boilerplate of the project using `create-next-app` (similar to `create-react-app`).

- I implemented 2 different approaches to load the data but both of them use the same Component to render it:

  - Server Side: I implemented the `getServerSideProps` that Next provides us to load the data and pre-render this page before returning it to the client. 

  - Client Side: I implemented React 18 (with strict mode) Suspense component to wait until the data is ready to render the list. Also I added Redux Toolkit which is intended to be the standard way to write Redux (Redux is a predictable state container for JavaScript apps) logic.

  - Client Side 2: I implemented React 18 (with strict mode) and React Query (for fetching, caching and updating asynchronous data in React).

- Instead of `styled-components`, I decided to install `stitches` which I love and I think the syntax is better.

- Typescript, ESLint and Prettier are in the project too.

### Q) What's the command to start the frontend application locally?
You can use `docker-compose` or you can just follow the instructions in [frontend/README.md](./frontend/README.md) to run it.
Go to `localhost:3000` and select one of the approaches to fetch and render the list.

### Q) What libraries did you add to the backend? What are they used for?
The backend is very simple, just an express app with 2 endpoints.

- Express: A layer built on the top of Node that helps manage servers and routes.
- Dotenv: A module that loads environment variables from a `.env` file into `process.env`.
- Celebrate: An express middleware function that allows you to ensure that all of your inputs are correct before any handler function.
- Axios: A promise based HTTP client.
- Jest: A JavaScript testing framework.

### Q) What's the command to start the backend application locally?
You can use `docker-compose` or you can just follow the instructions in [backend/README.md](./backend/README.md) to run it.
Go to `localhost:4000/exchange/recommend?amount=1` to get the recommended exchange.

### Q) Any other comments we should read before evaluating your solution?
It takes some time to install the dependencies for the Next project (frontend) during `compose-docker up`, you won't see anything related to `frontend` in the terminal until it finishes.
Also there are 2 GIFs in the README file showing both projects.

---

# General:

### Q) If you had more time, what further improvements or new features would you add?
- Update relative imports to absolute imports.
- Unit tests in frontend, I do some complex calculations in `useCurrencies` that I would like test.
- More unit tests in backend, more test cases and more test files.
- Build and start the apps instead of using `npm run dev` in docker.

### Q) Which parts are you most proud of? And why?
- Frontend:
  - I was able to fetch the data in 2 really different ways and both work pretty well.
  - Suspense + Redux is not an easy and common implementation these days.
  - It isn't usual to filter and sort lists at the same time in the frontend, `useCurrencies` tries to avoid useless calculations in the client side.
  - Implementing `position: grid` for the list.

- Backend:
  - Given a list of `[price, size]` and a desire `amount`, `calculateExecutionPrice` returns the final execution price without even needing to read the entire array once. 
  - The `ExternalExchange` abstract class gives me an easy way to add more external exchanges in the project.


### Q) Which parts did you spend the most time with? What did you find most difficult?
The hardest thing in my opinion was `useCurrencies`, it isn't usual to filter and sort lists at the same time in the frontend, `useCurrencies` tries to avoid useless calculations in the client side.

### Q) How did you find the test overall? Did you have any issues or have difficulties completing? If you have any suggestions on how we can improve the test, we'd love to hear them.
I really like it and I think I was able to show my skills with it. The backend challange was quite interesting, I learnt a lot about order books.
