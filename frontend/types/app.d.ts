export type Currency = {
  code: string;
  createdAt: string;
  id: string;
  isSupportedInUS: boolean;
  isSuspended: boolean;
  name: string;
  precision: number;
  supportsTestMode: boolean;
  type: string;
  updatedAt: string;
};
