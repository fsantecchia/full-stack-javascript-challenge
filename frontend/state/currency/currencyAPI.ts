import { Currency } from '../../types/app';

type Response = Currency[];

const delay = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));

export const fetchCurrencies = async (): Promise<Response> => {
  //const response = await fetch('https://api.moonpay.com/v3/currencies')
  const response = await fetch('http://localhost:3000/data.json');
  const currencies: Response = await response.json();

  await delay(1500); // Add delay to be able to see the loading state

  return currencies;
};
