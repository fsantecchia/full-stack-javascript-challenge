import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

import { fetchCurrencies } from './currencyAPI';
import { Currency } from '../../types/app';

interface CurrencyState {
  currencies: Currency[];
}

const initialState = { currencies: [] } as CurrencyState;

export const fetchCurrenciesAsync = createAsyncThunk('currency/fetchCurrencies', async () => {
  const response = await fetchCurrencies();
  return response;
});

const currencySlice = createSlice({
  name: 'currency',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchCurrenciesAsync.fulfilled, (state, action) => {
      state.currencies = action.payload;
    });
  },
});

export default currencySlice.reducer;
