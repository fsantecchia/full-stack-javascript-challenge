import type { NextPage } from 'next';
import Head from 'next/head';

import { Container, Main, Title, Description, Grid, Card, CardTitle, CardText } from '../styles/pageStyled';

const Home: NextPage = () => {
  return (
    <Container>
      <Head>
        <title>Home Test (Frontend)</title>
        <meta name="description" content="Takehome test for candidates applying for a full-stack developer position" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Main>
        <Title>Frontend challenge</Title>

        <Description>Select approach</Description>
        <p>
          They use the same <code>MessageList</code> component but they fetch the data in a different way.
        </p>

        <Grid>
          <Card href="/server-side">
            <CardTitle>Server Side &rarr;</CardTitle>
            <CardText>Next.js</CardText>
          </Card>

          <Card href="/client-side">
            <CardTitle>Client Side &rarr;</CardTitle>
            <CardText>React Suspense + Redux Toolkit</CardText>
          </Card>

          <Card href="/client-side-2">
            <CardTitle>Client Side 2 &rarr;</CardTitle>
            <CardText>React Query</CardText>
          </Card>
        </Grid>
      </Main>
    </Container>
  );
};

export default Home;
