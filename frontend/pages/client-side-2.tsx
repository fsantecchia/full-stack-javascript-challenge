import { useState } from 'react';
import Head from 'next/head';

import Button from '../components/primitives/Button/Button';
import CurrencyListReactQuery from '../components/CurrencyListReactQuery/CurrencyListReactQuery';

import { Container, Main, Title, Description, ContainerY } from '../styles/pageStyled';

// Adding it here for simplicity
const CurrencyListReactQueryAsync = () => {
  const [isListVisible, setIsListVisible] = useState(false);

  if (!isListVisible) {
    return (
      <ContainerY>
        <Button onClick={() => setIsListVisible(true)}>Click me!</Button>
      </ContainerY>
    )
  }

  return <CurrencyListReactQuery />;
}

const ClientSidePage = () => {
  return (
    <Container>
      <Head>
        <title>Home Test (Frontend)</title>
        <meta name="description" content="Takehome test for candidates applying for a full-stack developer position" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Main>
        <Title>Currencies</Title>

        <Description>Client Side + React Query Approach</Description>

        <CurrencyListReactQueryAsync />
      </Main>
    </Container>
  );
};

export default ClientSidePage;
