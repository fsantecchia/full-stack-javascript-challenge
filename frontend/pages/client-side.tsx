import { Suspense, useState } from 'react';
import Head from 'next/head';

import Button from '../components/primitives/Button/Button';
import CurrencyListRedux from '../components/CurrencyListRedux/CurrencyListRedux';

import { Container, Main, Title, Description, ContainerY } from '../styles/pageStyled';

// Adding it here for simplicity
const CurrencyListReduxAsync = () => {
  const [isListVisible, setIsListVisible] = useState(false);

  if (!isListVisible) {
    return (
      <ContainerY>
        <Button onClick={() => setIsListVisible(true)}>Click me!</Button>
      </ContainerY>
    )
  }

  return (
    <Suspense fallback={<ContainerY>Suspense fallback: Loading...</ContainerY>}>
      <CurrencyListRedux />
    </Suspense>
  )
}

const ClientSidePage = () => {
  return (
    <Container>
      <Head>
        <title>Home Test (Frontend)</title>
        <meta name="description" content="Takehome test for candidates applying for a full-stack developer position" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Main>
        <Title>Currencies</Title>

        <Description>Client Side + Suspense + Redux Approach</Description>

        <CurrencyListReduxAsync />
      </Main>
    </Container>
  );
};

export default ClientSidePage;
