import { InferGetServerSidePropsType } from 'next';
import Head from 'next/head';

import type { Currency as CurrencyType } from '../types/app';
import CurrencyList from '../components/CurrencyList/CurrencyList';

import { Container, Main, Title, Description } from '../styles/pageStyled';

type Data = CurrencyType[];

export const getServerSideProps = async () => {
  //const response = await fetch('https://api.moonpay.com/v3/currencies')
  const response = await fetch('http://localhost:3000/data.json');
  const currencies: Data = await response.json();

  return {
    props: {
      currencies,
    },
  };
};

const ServerSidePage = (props: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  return (
    <Container>
      <Head>
        <title>Home Test (Frontend)</title>
        <meta name="description" content="Takehome test for candidates applying for a full-stack developer position" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Main>
        <Title>Currencies</Title>

        <Description>Server Side Approach</Description>

        <CurrencyList currencies={props.currencies} />
      </Main>
    </Container>
  );
};

export default ServerSidePage;
