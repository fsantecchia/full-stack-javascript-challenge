import { useState } from 'react';

import type { Currency } from '../types/app';

type SortingCriteria = 'alpha-name' | 'alpha-code' | 'random';
type FilteringCriteria = (keyof Currency)[];

/* Helpers */

const filterCurrencies = (currencies: Currency[], filteringCriteria: FilteringCriteria) => {
  return currencies.filter((currency) => {
    // Validate that all the properties are thruly
    for (const criteria of filteringCriteria) {
      if (!currency[criteria]) {
        return false;
      }
    }

    return true;
  });
};

const shuffleArray = (rawArray: any[]) => {
  const array = [...rawArray];

  // Fisher-Yates algorithm - O(n)
  for (let index = array.length - 1; index > 0; index -= 1) {
    const secondaryIndex = Math.floor(Math.random() * (index + 1));
    const temp = array[index];
    array[index] = array[secondaryIndex];
    array[secondaryIndex] = temp;
  }

  return array;
};

const sortCurrencies = (rawCurrencies: Currency[], sortingCriteria: SortingCriteria) => {
  const currencies = [...rawCurrencies];

  if (sortingCriteria === 'random') {
    // Use faster approach
    return shuffleArray(currencies);
  }

  // Sort array conventionally - O(n^2)
  return currencies.sort((currencyA, currencyB) => {
    if (sortingCriteria === 'alpha-code') {
      if (currencyA.code < currencyB.code) {
        return -1;
      }
      if (currencyA.code > currencyB.code) {
        return 1;
      }
    } else if (sortingCriteria === 'alpha-name') {
      if (currencyA.name < currencyB.name) {
        return -1;
      }
      if (currencyA.name > currencyB.name) {
        return 1;
      }
    }

    return 0;
  });
};

/* useCurrencies Hook */

const useCurrencies = (allCurrencies: Currency[]) => {
  const [currencies, setCurrencies] = useState<Currency[]>([...allCurrencies]);
  const [filteringCriteria, setFilteringCriteria] = useState<FilteringCriteria>([]);
  const [sortingCriteria, setSortingCriteria] = useState<SortingCriteria>('alpha-code');

  // Performace metrics
  const [filteredItemsMetric, setFilteredItemsMetric] = useState(0);
  const [sortedItemsMetric, setSortedItemsMetric] = useState(0);

  // Private methods
  const addFilteringCriteria = (newCriteria: keyof Currency) => {
    // Add new filter to current list of currencies
    const filteredCurrencies = currencies.filter((currency) => {
      return currency[newCriteria];
    });

    setCurrencies(filteredCurrencies);
    setFilteringCriteria((currentCriteria) => {
      return [...currentCriteria, newCriteria];
    });

    setFilteredItemsMetric(currencies.length);
    setSortedItemsMetric(0);
  };

  const removeFilteringCriteria = (criteria: keyof Currency) => {
    const newFilteringCriteria = filteringCriteria.filter((innerCriteria) => innerCriteria !== criteria);

    // After removing a filtering criteria we need to process (filter and sort) the whole list again
    const filteredCurrencies = filterCurrencies(allCurrencies, newFilteringCriteria);
    const sortedCurrencies =
      sortingCriteria === 'alpha-code' ? filteredCurrencies : sortCurrencies(filteredCurrencies, sortingCriteria);

    setFilteringCriteria(newFilteringCriteria);
    setCurrencies(sortedCurrencies);

    setFilteredItemsMetric(allCurrencies.length);
    setSortedItemsMetric(sortingCriteria === 'alpha-code' ? 0 : filteredCurrencies.length);
  };

  // Public methods
  const toggleFilteringCriteria = (newCriteria: keyof Currency) => {
    const isRemovingCriteria = filteringCriteria.includes(newCriteria);

    isRemovingCriteria ? removeFilteringCriteria(newCriteria) : addFilteringCriteria(newCriteria);
  };

  const sort = (newSortingCriteria: SortingCriteria) => {
    // Do not sort if the criteria did not change
    if (newSortingCriteria === sortingCriteria && newSortingCriteria !== 'random') {
      setSortedItemsMetric(0);
      return;
    }

    // Only sort current array of currencies
    const sortedCurrencies = sortCurrencies(currencies, newSortingCriteria);

    setSortingCriteria(newSortingCriteria);
    setCurrencies(sortedCurrencies);

    setFilteredItemsMetric(0);
    setSortedItemsMetric(sortedCurrencies.length);
  };

  return {
    data: currencies,

    toggleFilteringCriteria,
    filteringCriteria,
    filteredItemsMetric,

    sort,
    sortingCriteria,
    sortedItemsMetric,
  };
};

export default useCurrencies;
