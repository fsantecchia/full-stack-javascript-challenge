import { useRef } from 'react';

const PENDING_PROMISES: Record<string, ReturnType<typeof getSuspendablePromise>> = {};

// This is the official basic promise-wrapper implemented in React Suspense Demo:
const getSuspendablePromise = (promise: Promise<any>) => {
  let status: 'pending' | 'success' | 'error' = 'pending';
  let result: any;

  let suspender = promise.then(
    (data) => {
      status = 'success';
      result = data;
    },
    (error) => {
      status = 'error';
      result = error;
    },
  );

  return {
    read() {
      if (status === 'pending') {
        throw suspender;
      } else if (status === 'error') {
        throw result;
      } else if (status === 'success') {
        return result;
      }
    },
  };
};

const useSuspense = ({ key, promiseGenerator }: { key: string; promiseGenerator: () => Promise<any> }) => {
  const getPromise = () => {
    // Do not trigger a new promise if there is already one pending promise
    if (PENDING_PROMISES[key]) {
      return PENDING_PROMISES[key];
    }

    const promise = promiseGenerator();
    const suspendablePromise = getSuspendablePromise(promise);
    PENDING_PROMISES[key] = suspendablePromise;

    return suspendablePromise;
  };

  const resource = useRef(getPromise());

  return resource.current.read();
};

export default useSuspense;
