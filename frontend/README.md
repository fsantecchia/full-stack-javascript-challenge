This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

Run `npm install` or `yarn install` to install the dependencies. (remember that if you use yarn, you might need to update your IDE to make it work with .pnp files that replace node_modules)

Run the development server `npm run dev` or `yarn dev`

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## More

Yarn version: 3.2.2