import { createStitches } from '@stitches/react';

export const { styled, css, globalCss, keyframes, getCssText, theme, createTheme, config } = createStitches({
  media: {
    tablet: '(min-width: 768px)',
    desktop: '(min-width: 1024px)',
  },

  theme: {
    colors: {
      primary: 'gainsboro',
      secondary: 'lightgray',
    },

    space: {
      xsmall: '5px',
      small: '10px',
      medium: '15px',
      large: '25px',
      xlarge: '30px',
    },

    fontSizes: {
      xsmall: '12px',
      small: '14px',
      base: '16px',
      large: '23px',
    },

    fontWeights: {
      semiBold: '500',
      bold: '700',
    },
  },
});

export const globalStyles = globalCss({
  ':root': {
    boxSizing: 'border-box',
  },

  body: {
    backgroundColor: 'black',
    color: '$primary',
    fontFamily:
      '"-apple-system", "BlinkMacSystemFont", "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", "sans-serif"',
    '-webkit-font-smoothing': 'antialiased',
    '-moz-osx-font-smoothing': 'grayscale',
    fontSize: '$base',
    margin: 0,
    padding: 0,
  },

  a: {
    color: 'inherit',
    textDecoration: 'none',
  },

  ul: {
    padding: 0,
  },

  p: {
    margin: 0,
  },
});
