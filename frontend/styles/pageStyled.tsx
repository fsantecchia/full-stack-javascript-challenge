import { styled } from './stitches.config';

export const Container = styled('div', {
  padding: '0 $large',
});

export const ContainerY = styled('div', {
  padding: '$large 0',
});

export const Main = styled('main', {
  alignItems: 'center',
  display: 'flex',
  flex: 1,
  flexDirection: 'column',
  justifyContent: 'center',
  minHeight: '100vh',
  padding: '50px 0',
});

export const Title = styled('h1', {
  fontSize: '64px',
  margin: 0,
  textAlign: 'center',
});

export const Description = styled('p', {
  fontSize: '24px',
  margin: '$small 0',
  textAlign: 'center',
});

export const Grid = styled('div', {
  alignItems: 'center',
  display: 'flex',
  flexWrap: 'wrap',
  justifyContent: 'center',
  maxWidth: '800px',
});

export const Card = styled('a', {
  border: '1px solid #eaeaea',
  borderRadius: '10px',
  height: '100px',
  margin: '$medium',
  padding: '$medium',
  textAlign: 'left',
  textDecoration: 'none',
  transition: 'color 0.15s ease, border-color 0.15s ease',
  width: '250px',

  '&:hover': { color: '#0070f3', borderColor: '#0070f3', cursor: 'pointer' },
  '&:focus': { color: '#0070f3', borderColor: '#0070f3' },
});

export const CardTitle = styled('h2', {
  fontSize: '24px',
  margin: '0 0 $small 0',
});

export const CardText = styled('p', {
  fontSize: '18px',
  margin: '0',
});
