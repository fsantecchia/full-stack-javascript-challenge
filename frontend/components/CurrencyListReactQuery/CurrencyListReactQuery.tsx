import { useQuery } from '@tanstack/react-query';

import { fetchCurrencies } from '../../state/currency/currencyAPI';
import CurrencyList from '../CurrencyList/CurrencyList';
import { ContainerY } from '../../styles/pageStyled';

const CURRENCIES_QUERY = { 
  queryKey: ['currencies'], 
  queryFn: fetchCurrencies 
};

const CurrencyListReactQuery = () => {
  const { data: currencies, isLoading } = useQuery(CURRENCIES_QUERY);

  if (isLoading) {
    return <ContainerY>React Query fallback: Loading...</ContainerY>; 
  }

  // Sending currencies as a prop to be able to reuse the same component in all the scenarios
  return <CurrencyList currencies={currencies} />;
};

export default CurrencyListReactQuery;
