import { styled } from '../../styles/stitches.config';

export const Container = styled('div', {
  padding: '0 $large',
});

export const Filters = styled('div', {
  border: '1px solid $primary',
  borderRadius: '$spaces$xsmall',
  margin: '$medium 0',
  padding: '$medium',
});

export const SwitchGroup = styled('div', {});

export const ButtonGroup = styled('div', {
  display: 'grid',
  gridGap: '$large',
  gridTemplateColumns: 'repeat(1, 1fr)',
  marginTop: '$small',

  '@desktop': {
    gridTemplateColumns: 'repeat(3, 1fr)',
  },
});

export const Metrics = styled('div', {
  marginTop: '$small',
  textAlign: 'center',
});

export const CurrentState = styled('div', {
  marginTop: '$small',
  textAlign: 'center',
});

export const CurrenciesGrid = styled('div', {
  display: 'grid',
  gridGap: '$large',
  gridTemplateColumns: 'repeat(1, 1fr)',
  justifyItems: 'center',
  marginTop: '$large',

  '@tablet': {
    gridTemplateColumns: 'repeat(2, 1fr)',
  },

  '@desktop': {
    gridTemplateColumns: 'repeat(3, 1fr)',
  },
});
