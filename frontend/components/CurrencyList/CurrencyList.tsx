import Switch from '../../components/primitives/Switch/Switch';
import Button from '../../components/primitives/Button/Button';
import CurrencyCard from '../../components/CurrencyCard/CurrencyCard';
import useCurrencies from '../../hooks/useCurrencies';
import type { Currency } from '../../types/app';

import { Container, Filters, ButtonGroup, SwitchGroup, Metrics, CurrentState, CurrenciesGrid } from './styled';

type Props = {
  currencies: Currency[];
};

const CurrencyList = (props: Props) => {
  const currencies = useCurrencies(props.currencies);

  const renderCurrencies = () => {
    return currencies.data.map((currency) => {
      return <CurrencyCard key={currency.id} currency={currency} />;
    });
  };

  return (
    <Container>
      <Filters>
        <SwitchGroup>
          <Switch onChange={() => currencies.toggleFilteringCriteria('isSupportedInUS')}>
            Include currencies not supported in the US
          </Switch>
          <Switch onChange={() => currencies.toggleFilteringCriteria('supportsTestMode')}>
            Include currencies not available in test mode
          </Switch>
        </SwitchGroup>
        <ButtonGroup>
          <Button selected={currencies.sortingCriteria === 'alpha-code'} onClick={() => currencies.sort('alpha-code')}>
            Sort alphabethicaly by code
          </Button>
          <Button selected={currencies.sortingCriteria === 'alpha-name'} onClick={() => currencies.sort('alpha-name')}>
            Sort alphabethicaly by name
          </Button>
          <Button selected={currencies.sortingCriteria === 'random'} onClick={() => currencies.sort('random')}>
            Apply random shuffle
          </Button>
        </ButtonGroup>
      </Filters>

      <Metrics>
        <div>Last action metrics</div>
        <div>Proccessed items to filter list: {currencies.filteredItemsMetric}</div>
        <div>Sorted Items: {currencies.sortedItemsMetric}</div>
      </Metrics>

      <CurrentState>
        Displaying: {currencies.data.length} / {props.currencies.length} currencies
      </CurrentState>

      <CurrenciesGrid>{renderCurrencies()}</CurrenciesGrid>
    </Container>
  );
};

export default CurrencyList;
