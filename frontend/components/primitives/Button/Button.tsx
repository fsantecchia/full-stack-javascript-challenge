import { styled } from '@stitches/react';

const Button = styled('button', {
  backgroundColor: '$primary',
  border: '2px solid $primary',
  borderRadius: '$space$xsmall',
  color: 'black',
  fontFamily: 'unset',
  fontSize: '$small',
  fontWeight: '$semiBold',
  height: 35,
  padding: '0 $small',

  '&:hover': { backgroundColor: '$secondary', cursor: 'pointer' },

  variants: {
    selected: { true: { borderColor: 'red' } },
  },
});

export default Button;
