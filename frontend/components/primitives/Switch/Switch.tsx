import React from 'react';
import { styled } from '@stitches/react';
import * as SwitchPrimitive from '@radix-ui/react-switch';

const StyledSwitch = styled(SwitchPrimitive.Root, {
  all: 'unset',
  backgroundColor: '$primary',
  borderRadius: '9999px',
  height: 25,
  position: 'relative',
  width: 42,

  '&:focus': { boxShadow: `0 0 0 2px black` },
  '&:hover': { cursor: 'pointer' },
  '&[data-state="checked"]': { backgroundColor: '#53ec53' },
});

const StyledThumb = styled(SwitchPrimitive.Thumb, {
  backgroundColor: 'black',
  borderRadius: '9999px',
  display: 'block',
  height: 21,
  transform: 'translateX(2px)',
  transition: 'transform 100ms',
  width: 21,
  willChange: 'transform',

  '&[data-state="checked"]': { transform: 'translateX(19px)' },
});

export const Label = styled('label', {
  marginRight: '$small',
  userSelect: 'none',
});

export const Container = styled('div', { display: 'flex', alignItems: 'center', padding: '$small $xsmall' });

type Props = {
  children: string;
  onChange: (isChecked: boolean) => void;
};

const Switch = (props: Props) => (
  <Container>
    <Label>{props.children}</Label>
    <StyledSwitch defaultChecked onCheckedChange={props.onChange}>
      <StyledThumb />
    </StyledSwitch>
  </Container>
);

export default Switch;
