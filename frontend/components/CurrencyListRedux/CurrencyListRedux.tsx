import { useTypedSelector, useTypedDispatch } from '../../state/hooks';
import { fetchCurrenciesAsync } from '../../state/currency/currencySlice';
import useSuspense from '../../hooks/useSuspense';
import CurrencyList from '../CurrencyList/CurrencyList';

const CurrencyListRedux = () => {
  const dispatch = useTypedDispatch();

  // Trigger parent <Suspense> fallback until the promise finishes
  useSuspense({ key: 'CurrencyListAsync', promiseGenerator: () => dispatch(fetchCurrenciesAsync()).unwrap() });

  const currencies = useTypedSelector((state) => state.currency.currencies);

  // Sending currencies as a prop to be able to reuse the same component in all the scenarios
  return <CurrencyList currencies={currencies} />;
};

export default CurrencyListRedux;
