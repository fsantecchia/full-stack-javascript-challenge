import type { Currency as CurrencyType } from '../../types/app';

import { Container, PrimaryText, SecondaryText } from './styled';

type Props = {
  currency: CurrencyType;
};

const CurrencyCard = (props: Props) => {
  return (
    <Container>
      <PrimaryText>{props.currency.name}</PrimaryText>
      <SecondaryText fontWeight="bold">{props.currency.code.toUpperCase()}</SecondaryText>
      <SecondaryText>US support {props.currency.isSupportedInUS ? '✅' : '❌'}</SecondaryText>
      <SecondaryText>Test mode support {props.currency.supportsTestMode ? '✅' : '❌'}</SecondaryText>
    </Container>
  );
};

export default CurrencyCard;
