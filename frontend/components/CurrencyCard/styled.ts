import { styled } from '@stitches/react';

export const Container = styled('div', {
  alignItems: 'center',
  backgroundColor: '$primary',
  borderRadius: '$space$large',
  color: 'black',
  display: 'flex',
  flexDirection: 'column',
  fontSize: '$small',
  height: '100px',
  justifyContent: 'center',
  padding: '$small',
  width: '200px',

  '&:hover': {
    backgroundColor: '$secondary',
  },
});

export const PrimaryText = styled('span', {
  fontSize: '$base',
  fontWeight: '$bold',
  textAlign: 'center',
});

export const SecondaryText = styled('span', {
  fontSize: '$xsmall',

  variants: {
    fontWeight: {
      bold: { fontWeight: '$semiBold' },
    },
  },
});
