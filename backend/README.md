# Express & Typescript

## Setup:

Run `npm install` to install the dependencies.

[Optional] If you would like to change the default configs, copy `.env.sample` to `.env`.

## Usage:

`npm run dev` - Run the development server.

`npm test` - Run tests.

`npm run build` - Builds the server.

`npm start` - Runs the server.

## endpoints:

A `GET` request to `/exchange/recommend?amount=1` will respond with the best exchange to buy the given amount of BTC