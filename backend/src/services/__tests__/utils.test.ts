import type { ExecutionPriceResponse } from '../externalExchanges/ExternalExchange';
import { calculateExecutionPrice, findLowestPrice } from '../utils';

const ASKS = [
  ['5.0000', '1'],
  ['5.5000', '0.5'],
  ['7.0000', '0.75'],
];

describe('utils', () => {
  describe('calculateExecutionPrice', () => {
    it('should work using a piece of the first element', () => {
      const result = calculateExecutionPrice(ASKS, 0.5);
      expect(result.amount).toEqual(0.5);
      expect(result.price).toEqual(2.5);
    });

    it('should work using the first element', () => {
      const result = calculateExecutionPrice(ASKS, 1);
      expect(result.amount).toEqual(1);
      expect(result.price).toEqual(5);
    });

    it('should work using the first element and the second element', () => {
      const result = calculateExecutionPrice(ASKS, 1.5);
      expect(result.amount).toEqual(1.5);
      expect(result.price).toEqual(7.75);
    });

    it('should work using the first element, the second element and a piece of the third element', () => {
      const result = calculateExecutionPrice(ASKS, 2);
      expect(result.amount).toEqual(2);
      expect(result.price).toEqual(11.25);
    });

    it('should work using the first element, the second element and the third element', () => {
      const result = calculateExecutionPrice(ASKS, 2.25);
      expect(result.amount).toEqual(2.25);
      expect(result.price).toEqual(13);
    });

    it('should throw error when the amount can not be satisfied', () => {
      const result = calculateExecutionPrice(ASKS, 500);
      expect(result.error).toEqual(true);
    });
  });

  describe('findLowestPrice', () => {
    it('should return lowest price', () => {
      const data: ExecutionPriceResponse[] = [
        { amount: 2, exchange: 'FTX', pair: 'BTC/USDT', price: 46170.8958, unitPrice: 23085.4479 },
        { amount: 2, exchange: 'Binance', pair: 'BTCUSDT', price: 46225.537990199984, unitPrice: 23112.768995099992 },
        { amount: 2, exchange: 'Coinbase', pair: 'BTC-USDT', price: 46228.28, unitPrice: 23114.14 },
        { error: true, exchange: 'X', pair: 'X' },
      ];
      const { best } = findLowestPrice(data);

      expect(best.exchange).toEqual('FTX');
      expect(best.price).toEqual(46170.8958);
    });
  });
});
