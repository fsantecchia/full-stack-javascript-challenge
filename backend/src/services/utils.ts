import type { ExecutionPriceResponse } from './externalExchanges/ExternalExchange';

export const findLowestPrice = (
  array: ExecutionPriceResponse[],
): {
  best: ExecutionPriceResponse;
  sortedArray: ExecutionPriceResponse[];
} => {
  const sortedArray = [...array].sort((a, b) => {
    // if "price" does not exist, move the one with price above
    if (!a.price && !b.price) return 0;
    if (!a.price) return 1;
    if (!b.price) return -1;

    // use "price" to compare them
    if (a.price < b.price) return -1;
    if (a.price > b.price) return 1;

    return 0;
  });

  return {
    sortedArray,
    best: sortedArray[0],
  };
};

export const calculateExecutionPrice = (
  orders: (string | number)[][],
  requestedAmount: number,
):
  | {
      amount: number;
      price: number;
      unitPrice: number;
      error?: undefined;
    }
  | {
      error: true;
      price?: undefined;
      amount?: undefined;
    } => {
  let currentAmount = 0;
  let currentPrice = 0;

  for (const [rawLocalPrice, rawLocalSize] of orders) {
    const localPrice = Number(rawLocalPrice);
    const localSize = Number(rawLocalSize);

    if (currentAmount + localSize > requestedAmount) {
      const remainingAmountNecessary = requestedAmount - currentAmount;
      currentAmount += remainingAmountNecessary;
      currentPrice += remainingAmountNecessary * localPrice;

      // break "for" once satisfied
      break;
    } else {
      currentAmount += localSize;
      currentPrice += localSize * localPrice;
    }
  }

  if (currentAmount === requestedAmount) {
    return {
      amount: currentAmount,
      price: currentPrice,
      unitPrice: currentPrice / currentAmount,
    };
  }

  return {
    error: true,
  };
};
