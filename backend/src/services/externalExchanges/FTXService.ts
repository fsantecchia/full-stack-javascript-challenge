import { ExternalExchange } from './ExternalExchange';
import type { OrderBookResponse } from './ExternalExchange';

export class FTXServiceClass extends ExternalExchange {
  _baseApiUrl = 'https://ftx.com/api';
  _exchangeName = 'FTX';

  async _fetchOrderBook(pair: string) {
    const orderBook = await this._fetch<{ result: OrderBookResponse }>({
      endpoint: `/markets/${pair}/orderbook`,
      method: 'GET',
      params: {
        depth: 100,
      },
    });

    return orderBook.result;
  }

  async calculateBitcoinExecutionPrice(amount: number) {
    const pair = 'BTC/USDT';
    const result = await this._calculateExecutionPrice(pair, amount);

    return result;
  }
}

// export singleton
export const FTXService = new FTXServiceClass();
