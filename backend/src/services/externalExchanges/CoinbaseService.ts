import { ExternalExchange } from './ExternalExchange';
import type { OrderBookResponse } from './ExternalExchange';

export class CoinbaseServiceClass extends ExternalExchange {
  _baseApiUrl = 'https://api.exchange.coinbase.com';
  _exchangeName = 'Coinbase';

  async _fetchOrderBook(pair: string) {
    const orderBook = await this._fetch<OrderBookResponse>({
      endpoint: `/products/${pair}/book`,
      method: 'GET',
      params: {
        level: 3,
      },
    });

    return orderBook;
  }

  async calculateBitcoinExecutionPrice(amount: number) {
    const pair = 'BTC-USDT';
    const result = await this._calculateExecutionPrice(pair, amount);

    return result;
  }
}

// export singleton
export const CoinbaseService = new CoinbaseServiceClass();
