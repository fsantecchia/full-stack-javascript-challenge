import { ExternalExchange } from './ExternalExchange';
import type { OrderBookResponse } from './ExternalExchange';

export class BinanceServiceClass extends ExternalExchange {
  _baseApiUrl = 'https://api.binance.com/api/v3';
  _exchangeName = 'Binance';

  async _fetchOrderBook(pair: string) {
    const orderBook = await this._fetch<OrderBookResponse>({
      endpoint: '/depth',
      method: 'GET',
      params: {
        symbol: pair,
      },
    });

    return orderBook;
  }

  async calculateBitcoinExecutionPrice(amount: number) {
    const pair = 'BTCUSDT';
    const result = await this._calculateExecutionPrice(pair, amount);

    return result;
  }
}

// export singleton
export const BinanceService = new BinanceServiceClass();
