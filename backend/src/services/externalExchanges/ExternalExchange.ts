import axios from 'axios';

import * as Utils from '../utils';

export type OrderBookResponse = {
  asks: string[][];
};

export type ExecutionPriceResponse = ReturnType<typeof Utils.calculateExecutionPrice> & {
  exchange: string;
  pair: string;
};

export abstract class ExternalExchange {
  abstract _baseApiUrl: string;
  abstract _exchangeName: string;
  abstract _fetchOrderBook(pair: string): Promise<OrderBookResponse>;

  abstract calculateBitcoinExecutionPrice(amount: number): Promise<ExecutionPriceResponse>;

  async _fetch<ResponseData>({
    endpoint,
    method,
    params,
  }: {
    endpoint: string;
    method: 'GET' | 'POST';
    params: any;
  }): Promise<ResponseData> {
    const response = await axios({
      url: `${this._baseApiUrl}${endpoint}`,
      method,
      params,
    });

    return response.data;
  }

  async _calculateExecutionPrice(pair: string, amount: number): Promise<ExecutionPriceResponse> {
    const { asks } = await this._fetchOrderBook(pair);

    const executionResult = Utils.calculateExecutionPrice(asks, amount);

    return {
      ...executionResult,
      exchange: this._exchangeName,
      pair,
    };
  }
}
