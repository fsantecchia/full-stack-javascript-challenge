import cors from 'cors';
import express from 'express';
import helmet from 'helmet';
import morgan from 'morgan';
import { errors as celebrateErrorsMiddleware } from 'celebrate';

import { errorHandler } from './middlewares/errorHandler';
import { fourOhFour } from './middlewares/fourOhFour';
import { exchangeRouter, EXCHANGE_BASE_URL } from './controllers/exchange/router';
import config from './config';

const app = express();

// Apply middlewares first
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(
  cors({
    // @ts-ignore
    origin: config.clientOrigins[config.nodeEnv],
  }),
);
app.use(helmet());
app.use(morgan('tiny'));

// Apply routes before error handling
app.get('/', (request, response) => response.json({ message: 'Welcome!' }));
app.use(EXCHANGE_BASE_URL, exchangeRouter);

// Apply error handling last
app.use(celebrateErrorsMiddleware());
app.use(fourOhFour);
app.use(errorHandler);

export default app;
