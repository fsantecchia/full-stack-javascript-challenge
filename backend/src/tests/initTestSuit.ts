import axios from 'axios';
import AxiosMockAdapter from 'axios-mock-adapter';

import * as binance from './data/binance';
import * as coinbase from './data/coinbase';
import * as ftx from './data/ftx';

// This sets the mock adapter on the default instance
export const axiosMock = new AxiosMockAdapter(axios);

axiosMock.onGet('https://api.binance.com/api/v3/depth').reply(200, binance.orderBook);
axiosMock.onGet('https://api.exchange.coinbase.com/products/BTC-USDT/book').reply(200, coinbase.orderBook);
axiosMock.onGet('https://ftx.com/api/markets/BTC/USDT/orderbook').reply(200, ftx.orderBook);
