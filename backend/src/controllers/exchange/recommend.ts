import { RequestHandler } from 'express';

import { BinanceService, CoinbaseService, FTXService } from '../../services/externalExchanges';
import type { ExecutionPriceResponse } from '../../services/externalExchanges/ExternalExchange';
import * as Utils from '../../services/utils';

type Response = {
  best: ExecutionPriceResponse;
  complementaryData: ExecutionPriceResponse[];
};

type Endpoint = RequestHandler<null, Response, null, { amount: number }>;

export const recommend: Endpoint = async (request, response, next) => {
  try {
    const { amount } = request.query;

    const binanceExecution = await BinanceService.calculateBitcoinExecutionPrice(Number(amount));
    const coinbaseExecution = await CoinbaseService.calculateBitcoinExecutionPrice(Number(amount));
    const ftxExecution = await FTXService.calculateBitcoinExecutionPrice(Number(amount));

    const { sortedArray, best } = Utils.findLowestPrice([binanceExecution, coinbaseExecution, ftxExecution]);

    response.json({
      best,
      complementaryData: sortedArray,
    });
  } catch (error) {
    next(error);
  }
};
