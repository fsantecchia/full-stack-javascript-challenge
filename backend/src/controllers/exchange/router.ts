import express from 'express';
import { celebrate, Joi, Segments } from 'celebrate';

import { recommend } from './recommend';

export const EXCHANGE_BASE_URL = '/exchange';
export const exchangeRouter = express.Router();

// GET articles/
exchangeRouter.get(
  '/recommend',
  celebrate({
    [Segments.QUERY]: {
      amount: Joi.number().greater(0).required(),
    },
  }),
  recommend,
);
