import { recommend } from '../recommend';

describe('module controllers/recommend', () => {
  it('should return articles', async () => {
    const request: any = {
      query: {
        amount: 2,
      },
    };
    const response: any = {
      json: jest.fn(),
    };
    const next = jest.fn();

    await recommend(request, response, next);

    expect(response.json).toHaveBeenCalledTimes(1);
    expect(response.json).toHaveBeenCalledWith({
      best: { amount: 2, exchange: 'FTX', pair: 'BTC/USDT', price: 46170.8958, unitPrice: 23085.4479 },
      complementaryData: [
        { amount: 2, exchange: 'FTX', pair: 'BTC/USDT', price: 46170.8958, unitPrice: 23085.4479 },
        { amount: 2, exchange: 'Binance', pair: 'BTCUSDT', price: 46225.537990199984, unitPrice: 23112.768995099992 },
        { amount: 2, exchange: 'Coinbase', pair: 'BTC-USDT', price: 46228.28, unitPrice: 23114.14 },
      ],
    });
  });
});
