# Fabricio Santecchia - Take Home Test (Backend & Frontend)

Test requirements: [Take Home Test](./Take Home Test.md)

FAQ: [FOLLOW-UP](./FOLLOW-UP.md)

## Overview

This is a takehome test for candidates applying for a full-stack developer
position. It contains 3 sections: "Frontend", "Backend" and "Follow-up" which
together include a series of tests involving JavaScript, React, Node.js, HTML and CSS.

## How to run it

Use docker compose to run it

```bash
docker-compose up
```

The backend challenge installation is faster than the frontend challenge with NextJS. The frontend challenge takes about 1m or 2m to install all the depencies the first time you run it.

Frontend: localhost:3000

Backend: localhost:4000

## Frontend

![](./frontend.gif)

## Backend

![](./backend.gif)
